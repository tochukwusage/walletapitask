﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WalletAPI.Models.Enumerators;

namespace WalletAPI.Models.DTOs
{
    public class UserForRegistrationDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password required!")]
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Date { get; set; }
        public Gender Gender { get; set; }
    }
}
