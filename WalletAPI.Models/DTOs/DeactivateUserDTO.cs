﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WalletAPI.Models.DTOs
{
    public class DeactivateUserDTO
    {
        //can set this up with jwt later
        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        public bool IsActive { get; set; }

        public DeactivateUserDTO(bool isActive=false)
        {
            IsActive = isActive;
        }
    }
}
