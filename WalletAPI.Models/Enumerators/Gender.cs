﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WalletAPI.Models.Enumerators
{
    public enum Gender
    {
        Male = 1,
        Female,
        Other,
        PreferNotToSay
    }
}
